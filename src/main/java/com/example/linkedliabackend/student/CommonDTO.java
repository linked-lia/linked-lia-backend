package com.example.linkedliabackend.student;
import com.example.linkedliabackend.student.education.StudentEducationDTO;
import com.example.linkedliabackend.student.education.StudentEducationEntity;
import com.example.linkedliabackend.student.experience.StudentExperienceDTO;
import com.example.linkedliabackend.student.experience.StudentExperienceEntity;
import com.example.linkedliabackend.student.info.StudentInfoDTO;
import com.example.linkedliabackend.student.info.StudentInfoEntity;
import com.example.linkedliabackend.student.skills.StudentSkillDTO;
import com.example.linkedliabackend.student.skills.StudentSkillEntity;

import java.util.stream.Collectors;
public class CommonDTO {
    public StudentDTO toStudentDTO(StudentEntity studentEntity) {
        if (studentEntity.getStudentInfoEntity() != null) {
            StudentInfoDTO studentInfoDTO = new StudentInfoDTO(
                    studentEntity.studentInfoEntity.getId(),
                    studentEntity.studentInfoEntity.getPhone(),
                    studentEntity.studentInfoEntity.getAddress(),
                    studentEntity.studentInfoEntity.getPostcode(),
                    studentEntity.studentInfoEntity.getCity(),
                    studentEntity.studentInfoEntity.getLinkedIn(),
                    studentEntity.studentInfoEntity.getGitLab(),
                    studentEntity.studentInfoEntity.getBio());
            System.out.println("Experience = " + studentEntity.getStudentExperienceEntity());
            return new StudentDTO(
                    studentEntity.getId(),
                    studentEntity.getFirstName(),
                    studentEntity.getLastName(),
                    studentEntity.getProfilepic(),
                    studentEntity.getResume(),
                    studentInfoDTO,
                    studentEntity.getStudentExperienceEntity().stream()
                            .map(this::toStudentExperienceDTO)
                            .collect(Collectors.toList()),
                    studentEntity.getStudentEducationEntities().stream()
                            .map(this::toStudentEducationDTO)
                            .collect(Collectors.toList()),
                    studentEntity.getStudentSkillEntities().stream()
                            .map(this::skillToDTO)
                            .collect(Collectors.toList()));
        }
        return new StudentDTO(
                studentEntity.getId(),
                studentEntity.getFirstName(),
                studentEntity.getLastName(),
                studentEntity.getProfilepic(),
                studentEntity.getResume(),
                studentEntity.getStudentExperienceEntity().stream()
                        .map(this::toStudentExperienceDTO)
                        .collect(Collectors.toList()),
                studentEntity.getStudentEducationEntities().stream()
                        .map(this::toStudentEducationDTO)
                        .collect(Collectors.toList()),
                studentEntity.getStudentSkillEntities().stream()
                        .map(this::skillToDTO)
                        .collect(Collectors.toList()));
    }

    public StudentExperienceDTO toStudentExperienceDTO(StudentExperienceEntity studentExperienceEntity) {
        return new StudentExperienceDTO(
                studentExperienceEntity.getId(),
                studentExperienceEntity.getCompany(),
                studentExperienceEntity.getPosition(),
                studentExperienceEntity.getDescription(),
                studentExperienceEntity.getCity(),
                studentExperienceEntity.getStartDate(),
                studentExperienceEntity.getEndDate());
    }
    public StudentEducationDTO toStudentEducationDTO(StudentEducationEntity studentEducationEntity) {
        return new StudentEducationDTO(
                studentEducationEntity.getId(),
                studentEducationEntity.getSchool(),
                studentEducationEntity.getField(),
                studentEducationEntity.getDegree(),
                studentEducationEntity.getCity(),
                studentEducationEntity.getStartDate(),
                studentEducationEntity.getEndDate());
    }

    public StudentInfoDTO toInfoDTO(StudentInfoEntity studentInfoEntity) {
        return new StudentInfoDTO(studentInfoEntity.getId(),
                studentInfoEntity.getPhone(),
                studentInfoEntity.getAddress(),
                studentInfoEntity.getPostcode(),
                studentInfoEntity.getCity(),
                studentInfoEntity.getLinkedIn(),
                studentInfoEntity.getGitLab(),
                studentInfoEntity.getBio());
    }

    private StudentSkillDTO skillToDTO(StudentSkillEntity skillEntity) {
        return new StudentSkillDTO(
                skillEntity.getId(),
                skillEntity.getSkill(),
                skillEntity.getLevel());
    }
}

