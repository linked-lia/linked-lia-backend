package com.example.linkedliabackend.student;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

@AllArgsConstructor
@Value
public class CreateStudent {
    String firstName;
    String lastName;
    String password;
    String email;
}
