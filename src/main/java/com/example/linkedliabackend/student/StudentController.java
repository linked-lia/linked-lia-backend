package com.example.linkedliabackend.student;

import com.example.linkedliabackend.student.experience.StudentExperienceDTO;
import com.example.linkedliabackend.student.experience.StudentExperienceEntity;
import com.example.linkedliabackend.student.info.StudentInfoDTO;
import com.example.linkedliabackend.student.skills.StudentSkillDTO;
import com.example.linkedliabackend.student.skills.StudentSkillEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentController {

    @Autowired
    StudentService studentService;
    CommonDTO commonDTO=new CommonDTO();

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @PostMapping("/addstudent")
    public StudentDTO createNewStudent(@RequestBody CreateStudent createStudent) {
        return studentService.createNewStudent(createStudent)
                .map(commonDTO::toStudentDTO)
                .orElse(null);
    }

    @GetMapping("/allstudents")
    public List<StudentDTO> getAllStudents() {
        return studentService.getAllStudents()
                .stream()
                .map(commonDTO::toStudentDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}/find")
    public StudentDTO findStudent(@PathVariable("id") String id) {
        return studentService.findStudent(id)
                .map(commonDTO::toStudentDTO)
                .orElse(null);
    }

    @PutMapping("/{id}/update")
    public StudentDTO updateStudent(@PathVariable("id") String id, @RequestBody UpdateStudent updateStudent) {
        return studentService.updateStudent(id, updateStudent)
                .map(commonDTO::toStudentDTO)
                .orElse(null);
    }

    @DeleteMapping("/{id}/delete")
    public StudentDTO deleteStudent(@PathVariable("id") String id) {
        return studentService.deleteStudent(id)
                .map(commonDTO::toStudentDTO)
                .orElse(null);
    }

/*    private StudentDTO toStudentDTO(StudentEntity studentEntity) {

        if (studentEntity.getStudentInfoEntity() == null)
            return getStudentDTO(studentEntity);

        StudentInfoDTO studentInfoDTO = getStudentInfoDTO(studentEntity);
        return getStudentDTOWithInfo(studentEntity, studentInfoDTO);
    }

    private StudentDTO getStudentDTO(StudentEntity studentEntity) {
        return new StudentDTO(
                studentEntity.getId(),
                studentEntity.getFirstName(),
                studentEntity.getLastName(),
                studentEntity.getStudentExperienceEntity().stream()
                        .map(this::toStudentExperienceDTO)
                        .collect(Collectors.toList()
                        ),
                studentEntity.getStudentSkillEntities().stream()
                        .map(this::skillToDTO)
                        .collect(Collectors.toList())
        );
    }

    private StudentDTO getStudentDTOWithInfo(StudentEntity studentEntity, StudentInfoDTO studentInfoDTO) {
        return new StudentDTO(
                studentEntity.getId(),
                studentEntity.getFirstName(),
                studentEntity.getLastName(),
                studentInfoDTO,
                studentEntity.getStudentExperienceEntity().stream()
                        .map(this::toStudentExperienceDTO)
                        .collect(Collectors.toList()),
                studentEntity.getStudentSkillEntities().stream()
                        .map(this::skillToDTO)
                        .collect(Collectors.toList())
        );
    }

    private StudentInfoDTO getStudentInfoDTO(StudentEntity studentEntity) {
        return new StudentInfoDTO(
                studentEntity.studentInfoEntity.getId(),
                studentEntity.studentInfoEntity.getPhone(),
                studentEntity.studentInfoEntity.getAddress(),
                studentEntity.studentInfoEntity.getPostcode(),
                studentEntity.studentInfoEntity.getCity(),
                studentEntity.studentInfoEntity.getLinkedIn(),
                studentEntity.studentInfoEntity.getGitLab(),
                studentEntity.studentInfoEntity.getBio()
        );
    }

    private StudentExperienceDTO toStudentExperienceDTO(StudentExperienceEntity studentExperienceEntity) {
        return new StudentExperienceDTO(
                studentExperienceEntity.getId(),
                studentExperienceEntity.getCompany(),
                studentExperienceEntity.getPosition(),
                studentExperienceEntity.getDescription(),
                studentExperienceEntity.getCity(),
                studentExperienceEntity.getStartDate(),
                studentExperienceEntity.getEndDate());
    }

    private StudentSkillDTO skillToDTO(StudentSkillEntity skillEntity) {
        return new StudentSkillDTO(
                skillEntity.getId(),
                skillEntity.getSkill(),
                skillEntity.getLevel());
    }*/
}
