package com.example.linkedliabackend.student;


import com.example.linkedliabackend.student.education.StudentEducationDTO;
import com.example.linkedliabackend.student.experience.StudentExperienceDTO;
import com.example.linkedliabackend.student.info.StudentInfoDTO;
import com.example.linkedliabackend.student.skills.StudentSkillDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
public class StudentDTO {
    String id;
    String firstName;
    String lastName;
    byte[] profilepic;
    byte[] resume;
    StudentInfoDTO studentInfoDTO;
    List<StudentExperienceDTO> studentExperienceDTO;
    List<StudentEducationDTO> studentEducationList;
    List<StudentSkillDTO> studentSkillDTOs;

    public StudentDTO(String id, String firstName, String lastName,byte[] profilepic,byte[] resume) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilepic = profilepic;
        this.resume= resume;
    }

    public StudentDTO(String id, String firstName, String lastName,byte[] profilepic,byte[] resume, StudentInfoDTO studentInfoDTO) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilepic = profilepic;
        this.resume= resume;
        this.studentInfoDTO = studentInfoDTO;
    }

    public StudentDTO(String id, String firstName, String lastName,
                      byte[] profilepic, byte[] resume,
                      List<StudentExperienceDTO> studentExperienceList,
                      List<StudentEducationDTO> studentEducationList,
                      List<StudentSkillDTO> studentSkillDTOs) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilepic = profilepic;
        this.resume = resume;
        this.studentExperienceDTO = studentExperienceList;
        this.studentEducationList = studentEducationList;
        this.studentSkillDTOs = studentSkillDTOs;
    }

}
