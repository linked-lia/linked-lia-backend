package com.example.linkedliabackend.student;

import com.example.linkedliabackend.student.education.StudentEducationEntity;
import com.example.linkedliabackend.student.experience.StudentExperienceEntity;
import com.example.linkedliabackend.student.info.StudentInfoEntity;
import com.example.linkedliabackend.student.skills.StudentSkillEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "students")
public class StudentEntity {

    @Id
    String id;
    @Column(name = "firstname")
    String firstName;
    @Column(name = "lastname")
    String lastName;
    @Column(name = "password")
    String password;
    @Column(name = "email")
    String email;
    @Column(name = "profile_pic")
    @Lob
    byte[] profilepic;
    @Column(name = "student_resume")
    @Lob
    byte[] resume;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "studentinfo_id")
//    @JsonBackReference
    StudentInfoEntity studentInfoEntity;

    @OneToMany(mappedBy = "studentEntity", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
//    @JsonManagedReference
    List<StudentExperienceEntity> studentExperienceEntity;

    @OneToMany(mappedBy = "studentEntity",cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<StudentEducationEntity> studentEducationEntities;

    @OneToMany(mappedBy = "studentEntity", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<StudentSkillEntity> studentSkillEntities;


    public StudentEntity(String id, String firstName, String lastName, String password, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.profilepic = new byte[1];
        this.resume = new byte[1];
        this.studentExperienceEntity = new ArrayList<>();
        this.studentEducationEntities= new ArrayList<>();
        this.studentSkillEntities = new ArrayList<>();
    }

}
