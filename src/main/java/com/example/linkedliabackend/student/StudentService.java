package com.example.linkedliabackend.student;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class StudentService {

    StudentRepository studentRepository;

 /*   public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }*/


    public Optional<StudentEntity> createNewStudent(CreateStudent createStudent) {
        StudentEntity studentEntity = new StudentEntity(
                UUID.randomUUID().toString(),
                createStudent.getFirstName(),
                createStudent.getLastName(),
                createStudent.getPassword(),
                createStudent.getEmail()
        );
        studentRepository.save(studentEntity);
        return Optional.of(studentEntity);
    }

    public List<StudentEntity> getAllStudents() {
        return (List<StudentEntity>) studentRepository.findAll();
    }

    public Optional<StudentEntity> deleteStudent(String id) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        studentRepository.delete(studentEntity);
        return Optional.of(studentEntity);
    }

    public Optional<StudentEntity> findStudent(String id) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        assert studentEntity != null;
        return Optional.of(studentEntity);
    }

    public Optional<StudentEntity> updateStudent(String id, UpdateStudent updateStudent) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        studentEntity.setFirstName(updateStudent.getFirstName());
        studentEntity.setLastName(updateStudent.getLastName());
        studentEntity.setPassword(updateStudent.getPassword());
        studentEntity.setEmail(updateStudent.getEmail());
        return Optional.of(studentRepository.save(studentEntity));
    }
}
