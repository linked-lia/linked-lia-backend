package com.example.linkedliabackend.student;

import lombok.AllArgsConstructor;
import lombok.Value;


@Value
@AllArgsConstructor
public class UpdateStudent {
    String firstName;
    String lastName;
    String password;
    String email;
}
