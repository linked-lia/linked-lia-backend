package com.example.linkedliabackend.student.education;
import com.example.linkedliabackend.student.CommonDTO;
import com.example.linkedliabackend.student.StudentDTO;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentEducationController {

    StudentEducationService studentEducationService;
    CommonDTO commonDTO = new CommonDTO();

    public StudentEducationController(StudentEducationService studentEducationService) {
        this.studentEducationService = studentEducationService;
    }
    @PostMapping("/{id}/addeducation")
    public StudentEducationDTO addEducation(@PathVariable("id") String id, @RequestBody CreateStudentEducation createStudentEducation){
        return  studentEducationService.addEducation(id,createStudentEducation)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }
    @DeleteMapping("/{id}/educations/{eid}/")
    public StudentEducationDTO deleteEducation(@PathVariable("id") String id, @PathVariable("eid") String eid){
        return  studentEducationService.deleteEducation(id,eid)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }
    @PutMapping("/{id}/educations/{eid}/")
    public StudentEducationDTO updateEducation(@PathVariable("id") String id, @PathVariable("eid") String eid,
                                               @RequestBody UpdateStudentEducation updateStudentEducation){
        return  studentEducationService.updateEducation(id,eid,updateStudentEducation)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }
}

