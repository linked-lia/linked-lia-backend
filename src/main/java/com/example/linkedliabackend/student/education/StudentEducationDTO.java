package com.example.linkedliabackend.student.education;
import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public class StudentEducationDTO {
    String id;
    String school;
    String field;
    String degree;
    String city;
    String startDate;
    String endDate;
}

