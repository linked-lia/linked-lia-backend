package com.example.linkedliabackend.student.education;
import com.example.linkedliabackend.student.StudentEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "studenteducation")
public class StudentEducationEntity {
    @Id
    String id;
    @Column(name= "school")
    String school;
    @Column(name= "field")
    String field;
    @Column(name= "degree")
    String degree;
    @Column(name= "city")
    String city;
    @Column(name= "startDate")
    String startDate;
    @Column(name= "endDate")
    String endDate;

    @ManyToOne
    @JoinColumn(name = "studentEntity_id")
//    @JsonBackReference
    StudentEntity studentEntity;
}
