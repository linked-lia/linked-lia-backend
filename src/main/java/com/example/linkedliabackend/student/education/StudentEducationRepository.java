package com.example.linkedliabackend.student.education;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface StudentEducationRepository extends CrudRepository<StudentEducationEntity,String> {
}

