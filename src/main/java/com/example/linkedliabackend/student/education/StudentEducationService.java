package com.example.linkedliabackend.student.education;
import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;
@Service
public class StudentEducationService {
    StudentRepository studentRepository;
    StudentEducationRepository studentEducationRepository;

    public StudentEducationService(StudentRepository studentRepository, StudentEducationRepository studentEducationRepository) {
        this.studentRepository = studentRepository;
        this.studentEducationRepository = studentEducationRepository;
    }

    public  Optional<StudentEducationEntity> addEducation(String id, CreateStudentEducation createStudentEducation) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        StudentEducationEntity studentEducationEntity = new StudentEducationEntity(
                UUID.randomUUID().toString(),
                createStudentEducation.getSchool(),
                createStudentEducation.getField(),
                createStudentEducation.getDegree(),
                createStudentEducation.getCity(),
                createStudentEducation.getStartDate(),
                createStudentEducation.getEndDate(),
                studentEntity);
        return Optional.of(studentEducationRepository.save(studentEducationEntity));
    }
    @Transactional
    public  Optional<StudentEducationEntity> deleteEducation(String id, String eid) {
        StudentEducationEntity studentEducationEntity = studentEducationRepository.findById(eid).orElse(null);
        studentEducationRepository.deleteById(eid);
        return Optional.of(studentEducationEntity);
    }
    @Transactional
    public  Optional<StudentEducationEntity> updateEducation(String id, String eid, UpdateStudentEducation updateStudentEducation) {
        StudentEducationEntity studentEducationEntity = studentEducationRepository.findById(eid).orElse(null);
        studentEducationEntity.setSchool(updateStudentEducation.getSchool());
        studentEducationEntity.setField(updateStudentEducation.getField());
        studentEducationEntity.setDegree(updateStudentEducation.getDegree());
        studentEducationEntity.setCity(updateStudentEducation.getCity());
        studentEducationEntity.setStartDate(updateStudentEducation.getStartDate());
        studentEducationEntity.setEndDate(updateStudentEducation.getEndDate());
        return  Optional.of(studentEducationRepository.save(studentEducationEntity));

    }
}
