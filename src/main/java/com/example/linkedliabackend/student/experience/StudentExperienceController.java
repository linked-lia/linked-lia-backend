package com.example.linkedliabackend.student.experience;


import com.example.linkedliabackend.student.CommonDTO;
import com.example.linkedliabackend.student.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentExperienceController {
    @Autowired
    StudentService studentService;
    @Autowired
    StudentExperienceService studentExperienceService;
    CommonDTO commonDTO = new CommonDTO();

    public StudentExperienceController(StudentService studentService, StudentExperienceService studentExperienceService) {
        this.studentService = studentService;
        this.studentExperienceService = studentExperienceService;
    }

    @PostMapping("/{id}/addExp")
    public StudentExperienceDTO addExperience(@PathVariable ("id") String id, @RequestBody CreateStudentExperience createStudentExperience){
        return studentExperienceService.addExperience(id, createStudentExperience)
                .map(commonDTO::toStudentExperienceDTO)
                .orElse(null);
    }

    @DeleteMapping("/{id}/{exp_id}/delete")
    public StudentExperienceDTO deleteExperience(@PathVariable ("id") String id, @PathVariable("exp_id") String expId){
        return studentExperienceService.deleteExperience(id, expId)
                .map(commonDTO::toStudentExperienceDTO)
                .orElse(null);
    }

/*        private StudentExperienceDTO toStudentExperienceDTO(StudentExperienceEntity studentExperienceEntity){
        return new StudentExperienceDTO(
                studentExperienceEntity.getId(),
                studentExperienceEntity.getCompany(),
                studentExperienceEntity.getPosition(),
                studentExperienceEntity.getDescription(),
                studentExperienceEntity.getCity(),
                studentExperienceEntity.getStartDate(),
                studentExperienceEntity.getEndDate());
        }*/
}
