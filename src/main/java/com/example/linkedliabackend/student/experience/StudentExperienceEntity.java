package com.example.linkedliabackend.student.experience;

import com.example.linkedliabackend.student.StudentEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "studentexperience")
public class StudentExperienceEntity {
    @Id
    String id;
    @Column(name= "company")
    String company;
    @Column(name= "position")
    String position;
    @Column(name= "description")
    String description;
    @Column(name= "city")
    String city;
    @Column(name= "startDate")
    String startDate;
    @Column(name= "endDate")
    String endDate;

    @ManyToOne
    @JoinColumn(name = "studentEntity_id")
//    @JsonBackReference
    StudentEntity studentEntity;
}
