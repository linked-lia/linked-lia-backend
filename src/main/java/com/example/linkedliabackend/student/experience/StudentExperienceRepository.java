package com.example.linkedliabackend.student.experience;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentExperienceRepository extends CrudRepository<StudentExperienceEntity, String> {
}
