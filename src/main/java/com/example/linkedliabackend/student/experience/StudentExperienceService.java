package com.example.linkedliabackend.student.experience;


import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class StudentExperienceService {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    StudentExperienceRepository studentExperienceRepository;

    public StudentExperienceEntity updateExperience(String id, UpdateStudentExperience updateStudentExperience) {
        return null;
    }

    @Transactional
    public Optional<StudentExperienceEntity> addExperience(String id, CreateStudentExperience createStudentExperience) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        StudentExperienceEntity studentExperienceEntity = new StudentExperienceEntity(
                UUID.randomUUID().toString(),
                createStudentExperience.getCompany(),
                createStudentExperience.getPosition(),
                createStudentExperience.getDescription(),
                createStudentExperience.getCity(),
                createStudentExperience.getStartDate(),
                createStudentExperience.getEndDate(),
                studentEntity);

        return Optional.of(studentExperienceRepository.save(studentExperienceEntity));
    }

    public Optional<StudentExperienceEntity> deleteExperience(String id, String expId) {
        StudentEntity student = studentRepository.findById(id).orElse(null);
        StudentExperienceEntity studentExperience = studentExperienceRepository.findById(expId).orElse(null);
        assert student != null;
        assert studentExperience != null;
        if (student.getStudentExperienceEntity().contains(studentExperience)) {
            studentExperience.setStudentEntity(null);
            studentExperienceRepository.delete(studentExperience);
        return Optional.of(studentExperience);
        }
        return Optional.empty();
    }
}
