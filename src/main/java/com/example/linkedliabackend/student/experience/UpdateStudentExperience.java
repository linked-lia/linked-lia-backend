package com.example.linkedliabackend.student.experience;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStudentExperience {
    String company;
    String position;
    String description;
    String city;
    String startDate;
    String endDate;
}
