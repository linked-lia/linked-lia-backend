package com.example.linkedliabackend.student.info;


import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
public class StudentInfoDTO {
    String id;
    String phone;
    String address;
    String postcode;
    String city;
    String linkedIn;
    String gitLab;
    String bio;
}
