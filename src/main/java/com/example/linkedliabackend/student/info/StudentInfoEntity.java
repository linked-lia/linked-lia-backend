package com.example.linkedliabackend.student.info;

import com.example.linkedliabackend.student.StudentEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@Table(name="studentinfo")
@AllArgsConstructor
@NoArgsConstructor
public class StudentInfoEntity {
    @Id
    String id;
    @Column(name = "phone")
    String phone;
    @Column(name = "address")
    String address;
    @Column(name = "postcode")
    String postcode;
    @Column(name = "city")
    String city;
    @Column(name = "linkedIn")
    String linkedIn;
    @Column(name = "gitLab")
    String gitLab;
    @Column(name = "bio")
    String bio;

    @OneToOne(fetch = FetchType.EAGER,mappedBy = "studentInfoEntity", cascade = CascadeType.ALL)
//    @JsonManagedReference
    StudentEntity studentEntity;
}
