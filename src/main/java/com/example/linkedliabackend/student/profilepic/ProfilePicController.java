package com.example.linkedliabackend.student.profilepic;
import com.example.linkedliabackend.student.*;
import com.example.linkedliabackend.student.experience.StudentExperienceDTO;
import com.example.linkedliabackend.student.experience.StudentExperienceEntity;
import com.example.linkedliabackend.student.info.StudentInfoDTO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProfilePicController {
    @Autowired
    ProfilePicService profilePicService;
    CommonDTO commonDTO = new CommonDTO();

    public ProfilePicController(ProfilePicService profilePicService) {
        this.profilePicService = profilePicService;
    }

    @PostMapping(value = "/{id}/addprofilepic",consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE})
    public StudentDTO addProfilePic(@PathVariable("id") String id, @RequestPart("file") MultipartFile multipartFile) throws IOException {
        StudentEntity studentEntity = profilePicService.addProfilePic(id,multipartFile);
        return commonDTO.toStudentDTO(studentEntity);
    }

}
