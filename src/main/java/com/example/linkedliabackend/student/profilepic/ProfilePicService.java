package com.example.linkedliabackend.student.profilepic;
import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
@Service

public class ProfilePicService {
    StudentRepository studentRepository;

    public ProfilePicService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Transactional
    public StudentEntity addProfilePic(String id, MultipartFile multipartFile) throws IOException {
        StudentEntity studentEntity= studentRepository.findById(id).orElse(null);
        studentEntity.setProfilepic(multipartFile.getBytes());
        studentRepository.save(studentEntity);
        return studentEntity;
    }
}
