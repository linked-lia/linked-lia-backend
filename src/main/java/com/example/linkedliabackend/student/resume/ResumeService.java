package com.example.linkedliabackend.student.resume;
import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import java.io.IOException;
@Service
public class ResumeService {
    StudentRepository studentRepository;

    public ResumeService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @Transactional
    public StudentEntity addResume(String id, MultipartFile multipartFile)throws IOException {
        StudentEntity studentEntity= studentRepository.findById(id).orElse(null);
        studentEntity.setResume(multipartFile.getBytes());
        studentRepository.save(studentEntity);
        return studentEntity;
    }
}
