package com.example.linkedliabackend.student.skills;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class CreateStudentSkill {
    String skill;
    SkillLevel level;
}
