package com.example.linkedliabackend.student.skills;


public enum SkillLevel {
    BEGINNER,
    MODERATE,
    EXPERT
}
