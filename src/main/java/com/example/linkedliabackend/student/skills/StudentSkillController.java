package com.example.linkedliabackend.student.skills;


import com.example.linkedliabackend.student.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentSkillController {
    @Autowired
    StudentService studentService;
    @Autowired
    StudentSkillService studentSkillService;

    @PostMapping("/{id}/add_skill")
    public StudentSkillDTO addSkill(@PathVariable("id") String id, @RequestBody CreateStudentSkill createStudentSkill) {
        return studentSkillService.addSkillToStudent(id, createStudentSkill)
                .map(this::skillToDTO)
                .orElse(null);
    }

    @PostMapping("/{id}/{skill_id}/update_skill")
    public StudentSkillDTO updateSkill(@PathVariable("id") String id,
                                       @PathVariable("skill_id") String skillId,
                                       @RequestBody CreateStudentSkill updateSkill){
        return studentSkillService.updateStudentSkill(id, skillId, updateSkill)
                .map(this::skillToDTO)
                .orElse(null);
    }

    @DeleteMapping("/{id}/{skill_id}/delete_skill")
    public StudentSkillDTO deleteSkill(@PathVariable("id") String id, @PathVariable("skill_id") String skillId) {
        return studentSkillService.deleteSkillFromStudent(id, skillId)
                .map(this::skillToDTO)
                .orElse(null);
    }

    private StudentSkillDTO skillToDTO(StudentSkillEntity skillEntity){
        return new StudentSkillDTO(
                skillEntity.getId(),
                skillEntity.getSkill(),
                skillEntity.getLevel());
    }
}
