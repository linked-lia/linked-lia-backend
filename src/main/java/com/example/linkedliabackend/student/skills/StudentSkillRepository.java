package com.example.linkedliabackend.student.skills;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentSkillRepository extends CrudRepository<StudentSkillEntity, String> {
}
