package com.example.linkedliabackend.student.skills;

import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class StudentSkillService {
    @Autowired
    StudentSkillRepository studentSkillRepository;
    @Autowired
    StudentRepository studentRepository;

    @Transactional
    public Optional<StudentSkillEntity> addSkillToStudent(String id, CreateStudentSkill createSkill) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        StudentSkillEntity skillEntity = new StudentSkillEntity(
                UUID.randomUUID().toString(),
                createSkill.getSkill(),
                createSkill.getLevel(),
                studentEntity);

        return Optional.of(studentSkillRepository.save(skillEntity));
    }

    public Optional<StudentSkillEntity> deleteSkillFromStudent(String id, String skillId) {
        StudentEntity student = studentRepository.findById(id).orElse(null);
        StudentSkillEntity skill = studentSkillRepository.findById(skillId).orElse(null);

        if(student.getId().equals(skill.getStudentEntity().getId())){
            skill.setStudentEntity(null);
            studentSkillRepository.delete(skill);
            return Optional.of(skill);
        }

        return Optional.empty();
    }

    public Optional<StudentSkillEntity> updateStudentSkill(String id, String skillId, CreateStudentSkill updateSkill) {
        StudentEntity student = studentRepository.findById(id).orElse(null);
        StudentSkillEntity skill = studentSkillRepository.findById(skillId).orElse(null);
        if(student.getId().equals(skill.getStudentEntity().getId())){
            skill.setSkill(updateSkill.getSkill());
            skill.setLevel(updateSkill.getLevel());
            return Optional.of(studentSkillRepository.save(skill));
        }

        return Optional.empty();
    }
}
