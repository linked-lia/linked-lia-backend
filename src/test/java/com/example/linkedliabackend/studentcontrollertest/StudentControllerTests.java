package com.example.linkedliabackend.studentcontrollertest;

import com.example.linkedliabackend.student.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@ActiveProfiles("integrationtest")
public class StudentControllerTests {
    @Autowired
    StudentController studentController;

    @Autowired
    StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        studentRepository.deleteAll();
    }

    @Test
    void test_addStudent_success() {
        //Given
        studentRepository.save(new StudentEntity("1","Shakir", "Zahedi",
                "12345", "ami@gmai.com"));

        // When
        List<StudentDTO> allStudents = studentController.getAllStudents();

        //Then
        assertEquals(1, allStudents.size());
        assertEquals("Shakir", allStudents.get(0).getFirstName());
    }

    @Test
    void test_update_student() {
        //Given
        StudentDTO student = studentController.createNewStudent(new CreateStudent("Majs", "Kolv",
                "12345", "ami@gmai.com"));

        //When
        studentController.updateStudent(student.getId(), new UpdateStudent("hej", "då", "12334", "jagödordögh"));
        StudentDTO updatedStudent = studentController.findStudent(student.getId());

        //Then
        assertEquals("hej", updatedStudent.getFirstName());
    }

}
